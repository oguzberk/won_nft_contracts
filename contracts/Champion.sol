// SPDX-License-Identifier: MIT LICENSE
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/utils/cryptography/MerkleProof.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/utils/Strings.sol";


//Staking contract
interface INeverEndingWar {
    function stakeDuringMint(address account, uint256 tokenId) external;
    function getStakedTokenIds(address account) external view returns (uint24[] memory);
}

//CombatPower address will be set at reveal. The contract holds combatPower and set infos of every champion
interface ICombatPower {
    function getChampionCP(uint256 tokenId) external pure returns (uint256);
    function getTeamChemistry(uint24[] calldata tokenIds) external pure returns (uint256);
    function getRawTokenSetData(uint tokenId) external pure returns(uint256 torsoId, uint256 legId, uint256 weaponId, bool isWeaponEnchanted);
}

contract Champion is
    ERC721,
    Ownable,
    ReentrancyGuard
{
    using Strings for uint256;
    using Counters for Counters.Counter;
    
    //for whitelisting
    bytes32 public merkleRoot;
    
    //for opensea listings and staking
    address proxyRegistryAddress;
    
    uint256 public maxSupply = 4444;
    //30 nfts are reserved for the team
    uint256 public availableTeamSupply = 30;
    //Will be updated at reveal
    string public baseURI = "";
    string public baseExtension = ".json";
    
    bool public paused = false;
    bool public presaleStarted = false;
    bool public publicSaleStarted = false;

    //WHITELIST//
    uint256 public whitelistWalletLimit = 1;
    uint256 public whitelistTxLimit = 1;
    //0.12 ETH IN WEI
    //uint256 whitelistPrice = 120000000000000000;
    //0.00012 for rinkeby
    uint256 public whitelistPrice = 120000000000000;

    //PUBLIC//
    uint256 public publicWalletLimit = 4444;
    uint256 public publicTxLimit = 10;
    //0.16 ETH IN WEI
    //uint256 publicPrice = 160000000000000000;
    //0 for testing
    uint256 public publicPrice = 0;

    //DUTCH AUCTION//
    bool public isDutchAuction = false;
    uint256 public dutchAuctionStartTime = 0;
    uint256 public dutchAuctionLength = 360 minutes;
    uint256 public dutchAuctionInterval = 30 minutes;
    
    mapping(address => uint256) public whitelistClaimed;
    mapping(address => uint256) public publicClaimed;
    
    Counters.Counter private tokenIdCounter;
    
    address combatPowerAddress;
    address neverEndingWarAddress;
    
    constructor(string memory uri, bytes32 root, address _proxyRegistryAddress)
    ERC721("Champion","CMP")
    ReentrancyGuard()
    {
        merkleRoot = root;
        proxyRegistryAddress = _proxyRegistryAddress;
        setBaseURI(uri);
    }
    
    //To prevent calls from contracts
    modifier onlyAccounts(){
        require(msg.sender == tx.origin, "Only actual accounts are allowed");
        _;
    }
    
    modifier isValidMerkleProof(bytes32[] calldata _proof){
        require(MerkleProof.verify(
            _proof,
            merkleRoot,
            keccak256(abi.encodePacked(msg.sender))
        ),"Invalid merkle proof");
        _;
    }

    function setBaseURI(string memory bURI) public onlyOwner{
        baseURI = bURI;
    }

    function setBaseExtension(string memory _baseExtension) external onlyOwner{
        baseExtension = _baseExtension;
    }
    
    function setNeverEndingWarAddress(address _neverEndingWarAddress) external onlyOwner{
        neverEndingWarAddress = _neverEndingWarAddress;
    }
    
    function setCombatPowerAddress(address _combatPowerAddress) external onlyOwner{
        combatPowerAddress = _combatPowerAddress;
    }

    function setIsDutchAuction(bool _isDutchAuction) external onlyOwner{
        isDutchAuction = _isDutchAuction;
    }

    function setDutchAuctionLength(uint256 _dutchAuctionLength) external onlyOwner{
        dutchAuctionLength = _dutchAuctionLength;
    }

    function setDutchAuctionInterval(uint256 _dutchAuctionInterval) external onlyOwner{
        dutchAuctionInterval = _dutchAuctionInterval;
    }
    
    function setPublicPrice(uint256 _publicPrice) external onlyOwner{
        publicPrice = _publicPrice;
    }

    function setMerkleRoot(bytes32 root) external onlyOwner{
        merkleRoot = root;
    }

    function togglePause() external onlyOwner {
        paused = !paused;
    }

    function togglePresale()external onlyOwner {
        presaleStarted = !presaleStarted;
    }

    function setPublicSale(bool _isActive) external onlyOwner {
        if(isDutchAuction && _isActive){
            dutchAuctionStartTime = block.timestamp;
        }
        publicSaleStarted = _isActive;
    }

    function getCombatPower(uint256 tokenId) external view returns (uint256) {
        if (combatPowerAddress == address(0x0)){
            return 0;
        }
        require(_exists(tokenId), "ERC721: owner query for nonexistent token");
        return ICombatPower(combatPowerAddress).getChampionCP(tokenId);
    }

    function getTeamChemistry(uint24[] calldata tokenIds) external view returns(uint256){
        if (combatPowerAddress == address(0x0)){
            return 0;
        }
        return ICombatPower(combatPowerAddress).getTeamChemistry(tokenIds);
    }

    //Mainly for Dutch auction
    function getPublicSalePrice()
    public
    view
    returns (uint256)
    {
        if (!isDutchAuction){
            return publicPrice;
        }
        if (block.timestamp < dutchAuctionStartTime) {
            return publicPrice;
        }
        if (block.timestamp - dutchAuctionStartTime >= dutchAuctionLength) {
            return whitelistPrice;
        } else {
            uint256 steps = (block.timestamp - dutchAuctionStartTime) / dutchAuctionInterval;
            return publicPrice - (steps * ((publicPrice - whitelistPrice) / (dutchAuctionLength / dutchAuctionInterval)));
        }
    }

    function teamMint(uint256 amount, address mintAddress) onlyOwner external
    {
        uint current = tokenIdCounter.current();
        require(current+amount <= maxSupply,       "Sold out.");
        require(availableTeamSupply-amount > 0,    "No available team supply.");
        availableTeamSupply -= amount;

        for (uint i = 0; i<amount; i++){
            tokenIdCounter.increment();
            uint256 tokenId = tokenIdCounter.current();
            _safeMint(mintAddress,tokenId);
        }
    }

    function whitelistMint(uint256 amount, bytes32[] calldata _proof, bool stake)
    external
    payable
    isValidMerkleProof(_proof)
    onlyAccounts {
        require(presaleStarted,                                                 "Presale is not active.");
        require(!paused,                                                        "Contract is paused.");
        if (stake){
            require(neverEndingWarAddress != address(0x0) ,                     "Staking contract is not set.");
        }
        require(amount<=whitelistTxLimit,                                       "Exceeded tx limit.");
        require(whitelistClaimed[msg.sender]+amount <= whitelistWalletLimit,    "Exceeded wallet limit.");
        
        uint current = tokenIdCounter.current();
        
        require(current+amount <= maxSupply,                                    "Sold out.");
        require(whitelistPrice * amount <= msg.value,                           "Not enough ethers sent.");
        
        whitelistClaimed[msg.sender] += amount;
        
        for (uint i = 0; i<amount; i++){
            mintInternal(stake);
        }
    }
    
    function publicMint(uint256 amount, bool stake)
    external
    payable
    onlyAccounts{
        uint256 price;
        price = getPublicSalePrice();
        require(publicSaleStarted,                                              "Public sale is not active.");
        require(!paused,                                                        "Contract is paused.");
        if (stake){
            require(neverEndingWarAddress != address(0x0) ,                     "Staking contract is not set.");
        }
        require(amount<=publicTxLimit,                                          "Exceeded tx limit.");
        require(publicClaimed[msg.sender]+amount <= publicWalletLimit,          "Exceeded wallet limit.");
        
        uint current = tokenIdCounter.current();

        require(current+amount <= maxSupply,                                    "Sold out.");
        require(price * amount <= msg.value,                                    "Not enough ethers sent.");

        publicClaimed[msg.sender] += amount;

        for (uint i = 0; i<amount; i++){
            mintInternal(stake);
        }
    }

    function mintInternal(bool stake) internal nonReentrant{
        tokenIdCounter.increment();
        uint256 tokenId = tokenIdCounter.current();
        if (stake){
            _safeMint(neverEndingWarAddress,tokenId);
            INeverEndingWar(neverEndingWarAddress).stakeDuringMint(msg.sender,tokenId);
        }else{
            _safeMint(msg.sender,tokenId);
        }
    }
    

    // should never be used inside of transaction because of gas fee
    function getUnstakedTokenIds(address account) public view returns (uint256[] memory ownerTokens) {
        uint256 total = totalSupply();
        uint256[] memory tmp = new uint256[](total);
        uint256 index = 0;
        for(uint tokenId = 1; tokenId <= total; tokenId++) {
            if (ownerOf(tokenId) == account) {
                tmp[index] = tokenId;
                index +=1;
            }
        }

        uint256[] memory tokens = new uint256[](index);
        for(uint i = 0; i < index; i++) {
            tokens[i] = tmp[i];
        }

        return tokens;
    }
    
    function getTokenSetData(uint256 tokenId) public view returns(uint256 torsoId, uint256 legId, uint256 weaponId, bool isWeaponEnchanted){
        return ICombatPower(combatPowerAddress).getRawTokenSetData(tokenId);
    }

    function _baseURI() internal view override returns (string memory){
        return baseURI;
    }
    
    function tokenURI(uint256 tokenId)
    public view virtual override returns (string memory){
        require(_exists(tokenId), "Token does not exist.");
        string memory currentBaseURI = _baseURI();
        return 
            bytes(currentBaseURI).length > 0 
                ? string(
                    abi.encodePacked(
                        currentBaseURI,
                        tokenId.toString(),
                        baseExtension
                    )
                )
                : "";
    }
    
    function totalSupply() public view returns (uint){
        return tokenIdCounter.current();
    }
    
    //For opensea listings and staking(neverendingwar)
    function isApprovedForAll(address owner, address operator)
    override public view returns (bool){
        ProxyRegistry proxyRegistry = ProxyRegistry(proxyRegistryAddress);
        if (address(proxyRegistry.proxies(owner)) == operator){
            return true;
        }
        
        if (neverEndingWarAddress == operator){
            return true;
        }
        return super.isApprovedForAll(owner,operator);
    }
}

contract OwnableDelegateProxy{}
contract ProxyRegistry{
    mapping(address => OwnableDelegateProxy) public proxies;
}