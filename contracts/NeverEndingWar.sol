// SPDX-License-Identifier: MIT LICENSE

pragma solidity ^0.8.9;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721Receiver.sol";
import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";

interface IChampion {
    function ownerOf(uint256 tokenId) external view returns (address);
    function transferFrom(
        address from,
        address to,
        uint256 tokenId
    ) external;
    function getCombatPower(uint256 tokenId) external view returns (uint256);
    function getTeamChemistry(uint24[] calldata tokenIds) external view returns(uint256);
    function totalSupply() external view returns (uint);
}

interface IWoNToken {
    function mint(address to, uint256 amount) external;
}

contract NeverEndingWar is Ownable, IERC721Receiver {
    uint256 public totalStaked;
    address championAddress;
    address wonTokenAddress;

    //Stores staking information of a user
    struct UserStake {
        //Staked tokens
        uint24[] tokenIds;
        //Timestamp of last stake/unstake transaction
        uint48 timestamp;
        //Total unclaimed/calculated won token of a user
        uint184 totalWoNToken;
    }
    
    struct Stake {
        uint24 tokenId;
        address owner;
    }

    event ChampionStaked(address owner, uint256 tokenId, uint256 value);
    event ChampionUnstaked(address owner, uint256 tokenId, uint256 value);
    event Claimed(address owner, uint256 amount);
    

    mapping(address => UserStake) public userTeamData;
    //to match tokenIds with owners
    mapping(uint256 => Stake) public vault;

    constructor(address _championAddress, address _tokenAddress) {
        championAddress = _championAddress;
        wonTokenAddress = _tokenAddress;
    }

    //Lets minters to stake their tokens during mint
    function stakeDuringMint(address account, uint256 tokenId) external {
        require(msg.sender == address(championAddress), 'Can be called only by NFT contract');
        totalStaked ++;
        if(userTeamData[account].timestamp == 0){
            userTeamData[account] = UserStake({
            tokenIds: new uint24[](0),
            timestamp: 0,
            totalWoNToken : 0
            });
        }else{
            updateTotalWon(account);
        }
        userTeamData[account].timestamp = uint48(block.timestamp);
        require(IChampion(championAddress).ownerOf(tokenId) == address(this), "nft must be sent first");
        require(vault[tokenId].tokenId == 0, 'already staked');
        vault[tokenId] = Stake({
            owner: account,
            tokenId: uint24(tokenId)
        });

        userTeamData[account].tokenIds.push() = uint24(tokenId);
        emit ChampionStaked(account, tokenId, block.timestamp);
        
    }
    
    function stake(uint256[] calldata tokenIds) external {
        uint256 tokenId;
        totalStaked += tokenIds.length;
        
        if(userTeamData[msg.sender].timestamp == 0){
            userTeamData[msg.sender] = UserStake({
                tokenIds: new uint24[](0),
                timestamp: 0,
                totalWoNToken : 0
            });
        }else{
            updateTotalWon(msg.sender);
        }
        userTeamData[msg.sender].timestamp = uint48(block.timestamp);
        
        for (uint i = 0; i < tokenIds.length; i++) {
            tokenId = tokenIds[i];
            require(IChampion(championAddress).ownerOf(tokenId) == msg.sender, "not your token");
            require(vault[tokenId].tokenId == 0, 'already staked');

            IChampion(championAddress).transferFrom(msg.sender, address(this), tokenId);
            emit ChampionStaked(msg.sender, tokenId, block.timestamp);

            vault[tokenId] = Stake({
                owner: msg.sender,
                tokenId: uint24(tokenId)
            });
            userTeamData[msg.sender].tokenIds.push() = uint24(tokenId);
        }
    }

    function _unstakeMany(address account, uint256[] calldata tokenIds) internal {
        uint256 tokenId;
        totalStaked -= tokenIds.length;

        updateTotalWon(msg.sender);
        userTeamData[account].timestamp = uint48(block.timestamp);
        
        for (uint i = 0; i < tokenIds.length; i++) {
            tokenId = tokenIds[i];
            Stake memory staked = vault[tokenId];
            require(staked.owner == msg.sender, "not an owner");
            delete userTeamData[account].tokenIds[tokenId];
            delete vault[tokenId];
            emit ChampionUnstaked(account, tokenId, block.timestamp);
            IChampion(championAddress).transferFrom(address(this), account, tokenId);
        }
    }

    function claim() external {
        _claim  (msg.sender);
    }

    function claimForAddress(address account) external {
        _claim(account);
    }

    function unstake(uint256[] calldata tokenIds) external {
        _unstakeMany(msg.sender,tokenIds);
    }

    function _claim(address account) internal {
        uint256 claimableWon = getClaimableWon(account);
        require(claimableWon>0,     "You don't have any claimable Won.");
        userTeamData[account].timestamp = uint48(block.timestamp);
        userTeamData[account].totalWoNToken = 0;
        IWoNToken(wonTokenAddress).mint(account, claimableWon);
        emit Claimed(account, claimableWon);
    }

    function updateTotalWon(address account) internal{
        uint256 earned = getClaimableWon(account);
        userTeamData[account].totalWoNToken = uint184(earned);
    }
    
    function getClaimableWon(address account) public view returns(uint256){
        uint256 earned = userTeamData[account].totalWoNToken;
        uint256 teamCp = getTeamCp(account);
        
        //Claimable won is equal to totalWon already recorded in userTeamData + the new won generated since the last recorded timestamp.
        earned += ((teamCp + (teamCp * getTeamChemistry(account))/100)* getTimePassedSinceLastUpdate(account)) /1 days;
        return earned;
    }
    
    function getTeamCp(address account) public view returns(uint256){
        uint256 teamCp = 0;
        for (uint i = 0; i < userTeamData[account].tokenIds.length; i++) {
            uint24 tokenId = userTeamData[account].tokenIds[i];
            teamCp += IChampion(championAddress).getCombatPower(tokenId);
        }
        return teamCp;
    }
    
    function getTimePassedSinceLastUpdate(address account) public view returns(uint256){
        return (block.timestamp - userTeamData[account].timestamp);
    }
    
    function getTeamChemistry(address account) public view returns(uint256){
        return IChampion(championAddress).getTeamChemistry(userTeamData[account].tokenIds);
    }

    function balanceOf(address account) public view returns (uint256) {
        return userTeamData[account].tokenIds.length;
    }
    
    function getStakedTokenIds(address account) external view returns (uint24[] memory){
        uint length = userTeamData[account].tokenIds.length;
        uint24[] memory tokenIds = new uint24[](length);
        for (uint i=0; i<length; i++){
            tokenIds[i] = userTeamData[account].tokenIds[i];
        }
        return tokenIds;
    }

    function onERC721Received(
        address,
        address from,
        uint256,
        bytes calldata
    ) external pure override returns (bytes4) {
        require(from == address(0x0), "Cannot transfer directly.");
        return IERC721Receiver.onERC721Received.selector;
    }

}