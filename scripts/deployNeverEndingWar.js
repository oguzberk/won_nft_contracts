const hre = require("hardhat");
//const championAddress = "0xE74DBA3bd8Acf46aFdF94247e1088Fb09dB5e7e5";
//const wonTokenAddress = "0x9D6dd8686375205f3A4eE3cB4d06d7feeC8D547D";

async function main(championAddress, wonTokenAddress) {
    const NeverEndingWar = await hre.ethers.getContractFactory("NeverEndingWar");
    const neWar = await NeverEndingWar.deploy(championAddress, wonTokenAddress);
    await neWar.deployed();
    console.log("NeverEndingWar deployed to: ", neWar.address);
    return neWar.address;
    
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });