const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan")

async function main() {
    await hre.run("verify:verify",{
        address: "0x9D6dd8686375205f3A4eE3cB4d06d7feeC8D547D",
        constructorArguments:[],
    })
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
