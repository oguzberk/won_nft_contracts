const hre = require("hardhat");
const {MerkleTree} = require("merkletreejs");
const keccak256 = require("keccak256");
const whitelist = require("./whitelist.js");
require("@nomiclabs/hardhat-etherscan")
const baseURI = "";
//rinkeby
const proxyRegistryAddress = "0xf57b2c51ded3a29e6891aba85459d600256cf317";
//mainnet
//const proxyRegistryAddress = 0xa5409ec958c83c3f309868babaca7c86dcb077c1;


async function main() {
    const leafNodes = whitelist.map(addr => keccak256(addr));
    const merkleTree = new MerkleTree(leafNodes,keccak256,{sortPairs:true});
    const root = merkleTree.getRoot();
    
    await hre.run("verify:verify",{
        address: "0xE74DBA3bd8Acf46aFdF94247e1088Fb09dB5e7e5",
        constructorArguments:[baseURI,root,proxyRegistryAddress],
    })
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
