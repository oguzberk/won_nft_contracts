const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan")

async function main(addrs) {
    await hre.run("verify:verify",{
        address: addrs,
        constructorArguments:[],
    })
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
