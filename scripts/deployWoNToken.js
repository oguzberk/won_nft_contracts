const hre = require("hardhat");

async function main() {
    const WoNToken = await hre.ethers.getContractFactory("WoNToken");
    const won = await WoNToken.deploy();
    await won.deployed();
    console.log("WoNToken deployed to: ", won.address);
    return won.address;
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });