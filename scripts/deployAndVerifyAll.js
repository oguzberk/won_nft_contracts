const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan")
const {MerkleTree} = require("merkletreejs");
const keccak256 = require("keccak256");
const whitelist = require("./whitelist.js");
const baseURI = "";
//rinkeby
const proxyRegistryAddress = "0xf57b2c51ded3a29e6891aba85459d600256cf317";
//mainnet
//const proxyRegistryAddress = 0xa5409ec958c83c3f309868babaca7c86dcb077c1;

const delay = ms => new Promise(res => setTimeout(res, ms));

async function deployAll(){
    let combatPowerAddress = await deployCombatPower();
    let championsAddress = await deployChampions();
    let wonTokenAddress = await deployWonToken();
    let neWarAddress = await deployNeverEndingWar(championsAddress,wonTokenAddress);
    console.log("Deployed all contracts.");
    console.log("Waiting before verifying...");
    await delay(1000 * 60);
    console.log("Verifying CombatPower contract...");
    await verifyCombatPower(combatPowerAddress).then()
        .catch((error) => {
            console.error(error);
        });
    console.log("Verifying Champions contract...");
    await verifyChampions(championsAddress).then()
        .catch((error) => {
            console.error(error);
        });
    console.log("Verifying WonToken contract...");
    await verifyWonToken(wonTokenAddress).then()
        .catch((error) => {
            console.error(error);
        });
    console.log("Verifying NeverEndingWar contract...");
    await verifyNeverEndingWar(neWarAddress,championsAddress,wonTokenAddress).then()
        .catch((error) => {
            console.error(error);
        });
    console.log("All contracts deployed and verified!");
    console.log("CombatPower: "+combatPowerAddress);
    console.log("Champions: "+championsAddress);
    console.log("WonToken: "+wonTokenAddress);
    console.log("NeverEndingWar: "+neWarAddress);
    
    
}

async function deployCombatPower(){
    const CombatPower = await hre.ethers.getContractFactory("CombatPower");
    const cp = await CombatPower.deploy();
    await cp.deployed();
    console.log("CombatPower deployed to: ", cp.address);
    return cp.address;
}

async function verifyCombatPower(addrs){
    await hre.run("verify:verify",{
        address: addrs,
        constructorArguments:[],
    })
}

async function deployChampions(){
    const leafNodes = whitelist.map(addr => keccak256(addr));
    const merkleTree = new MerkleTree(leafNodes,keccak256,{sortPairs:true});
    const root = merkleTree.getRoot();

    const Champions = await hre.ethers.getContractFactory("Champion");
    const champions = await Champions.deploy(baseURI,root,proxyRegistryAddress);
    await champions.deployed();
    console.log("Champions deployed to: ", champions.address);
    return champions.address
}

async function verifyChampions(addrs){
    const leafNodes = whitelist.map(addr => keccak256(addr));
    const merkleTree = new MerkleTree(leafNodes,keccak256,{sortPairs:true});
    const root = merkleTree.getRoot();
    
    await hre.run("verify:verify",{
        address: addrs,
        constructorArguments:[baseURI,root,proxyRegistryAddress],
    })
}

async function deployWonToken(){
    const WoNToken = await hre.ethers.getContractFactory("WoNToken");
    const won = await WoNToken.deploy();
    await won.deployed();
    console.log("WoNToken deployed to: ", won.address);
    return won.address;
}

async function verifyWonToken(addrs){
    await hre.run("verify:verify",{
        address: addrs,
        constructorArguments:[],
    })
}

async function deployNeverEndingWar(championAddress, wonTokenAddress){
    const NeverEndingWar = await hre.ethers.getContractFactory("NeverEndingWar");
    const neWar = await NeverEndingWar.deploy(championAddress, wonTokenAddress);
    await neWar.deployed();
    console.log("NeverEndingWar deployed to: ", neWar.address);
    return neWar.address;
}

async function verifyNeverEndingWar(addrs, championAddress, wonTokenAddress){
    await hre.run("verify:verify",{
        address: addrs,
        constructorArguments:[championAddress, wonTokenAddress],
    })
}

deployAll()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });