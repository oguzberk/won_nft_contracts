const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan")
const championAddress = "0xE74DBA3bd8Acf46aFdF94247e1088Fb09dB5e7e5";
const wonTokenAddress = "0x9D6dd8686375205f3A4eE3cB4d06d7feeC8D547D";


async function main() {
    await hre.run("verify:verify",{
        address: "0xb224Aeaad99fA2763c88c1e8b6ec4A03708859c7",
        constructorArguments:[championAddress, wonTokenAddress],
    })
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
