const hre = require("hardhat");
const {MerkleTree} = require("merkletreejs");
const keccak256 = require("keccak256");
const whitelist = require("./whitelist.js");
const baseURI = "";
//rinkeby
const proxyRegistryAddress = "0xf57b2c51ded3a29e6891aba85459d600256cf317";
//mainnet
//const proxyRegistryAddress = 0xa5409ec958c83c3f309868babaca7c86dcb077c1;


async function main() {
    const leafNodes = whitelist.map(addr => keccak256(addr));
    const merkleTree = new MerkleTree(leafNodes,keccak256,{sortPairs:true});
    const root = merkleTree.getRoot();
    
    const Champions = await hre.ethers.getContractFactory("Champion");
    const champions = await Champions.deploy(baseURI,root,proxyRegistryAddress);
    await champions.deployed();
    console.log("Champions deployed to: ", champions.address);
    return champions.address
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });