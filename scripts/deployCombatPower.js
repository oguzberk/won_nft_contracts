const hre = require("hardhat");

export async function main() {
    const CombatPower = await hre.ethers.getContractFactory("CombatPower");
    const cp = await CombatPower.deploy();
    await cp.deployed();
    console.log("CombatPower deployed to: ", cp.address);
    return cp.address;
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });